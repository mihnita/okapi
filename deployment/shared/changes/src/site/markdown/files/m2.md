# Changes from M1 to M2

## Filters

* Added the DTD Filter

* Added the PlainText Filter

* Added the Table Filter

* Others

    * Several pre-defined filter configurations have been added or
      updated: Mozilla-RDF, XML Android Strings, XML Java properties,
      RESX, Monoligual PO, SRT (Sub-titles), plain-text lines, plain-text
      paragraphs, CSV, etc.
    * The OpenXML Filter (DOCX, PPTX, XSLX files) has been improved and
      now provides much inline code simplification.
    * The definition of the parameters for the RegEx Filter have been
      modified to allow the support of target text, ID, etc. This new
      format is not compatible with the one of M1.
    * Other filters (HTML, Properties, XLIFF, TMX, PO, and OpenDocument
      filters) have been improved.

## Libraries

* A new TM connector to query remote GlobalSight TM servers has been
  added. (See the Java Example05 of the okapi-lib distribution for an
  illustration on how to use this component).
* A connector to query the remote OpenTran server has been added.
  (See the Java Example05 of the okapi-lib distribution for an
  illustration on how to use this component).
* New `RawDocument` object model.
* The events mechanism has been augmented to work with batch items
  in the pipeline.
* The encoding detection and handling of BOM has been modified in
  most filters and utilities.
* The pipeline mechanism has been extensively re-written.
* Many steps for the pipeline have been created, they are
  experimental for now.

## Applications

* Rainbow

    * The selection of the filter settings is now done using the new
      filter configuration mapping system integrated in the library.
    * An experimental interface for creating and executing pipelines has
      been added (see Utilities > Edit / Execute Pipeline)
    * The creation of OmegaT, XLIFF and RTF translation packages has
      been modified to handle pre-segmentation and pre-leveraging.
    * Uses the latest libraries.

* Ratel

    * Better preservation of comments in SRX files; and capability to
      add comments from within Ratel.
    * Uses the latest libraries.
