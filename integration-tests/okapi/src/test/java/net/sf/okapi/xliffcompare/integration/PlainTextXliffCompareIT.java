package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.plaintext.PlainTextFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class PlainTextXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_plaintext";
	private static final String DIR_NAME = "/plaintext/";
	private static final List<String> EXTENSIONS = Arrays.asList("txt");

	public PlainTextXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, PlainTextFilter::new);
	}

	@Test
	public void plainTextXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(CONFIG_ID, false, new FileComparator.XmlComparator());
	}
}
