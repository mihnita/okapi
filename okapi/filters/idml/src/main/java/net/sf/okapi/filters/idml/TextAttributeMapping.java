/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml;

import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnit;

import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import java.util.Collections;

final class TextAttributeMapping {
    private final IdGenerator textUnitIds;

    TextAttributeMapping(final IdGenerator textUnitIds) {
        this.textUnitIds = textUnitIds;
    }

    ITextUnit textUnitFor(final Element textElement, final QName attributeName) {
        final Attribute attribute = textElement.startElement().getAttributeByName(attributeName);
        final ITextUnit textUnit = new TextUnit(this.textUnitIds.createId());
        textUnit.setPreserveWhitespaces(true);
        textUnit.setSource(new TextContainer(new TextFragment(attribute.getValue())));
        final ISkeleton skeleton = new TextSkeleton(textElement, Collections.emptyMap(), attributeName);
        skeleton.setParent(textUnit);
        textUnit.setSkeleton(skeleton);
        return textUnit;
    }
}
