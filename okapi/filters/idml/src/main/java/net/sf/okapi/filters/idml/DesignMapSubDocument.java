/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.XMLEventsReader;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.skeleton.ZipSkeleton;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

final class DesignMapSubDocument implements SubDocument {
    private static final String TEXT_XML = "text/xml";
    private static final String CUSTOM_TEXT_VARIABLE_PREFERENCE = "CustomTextVariablePreference";
    private static final String PROPERTIES = "Properties";
    private static final String CONTENTS = "Contents";
    private static final String INDEX = "Index";
    private static final String TOPIC = "Topic";
    private static final QName NAME = new QName("Name");

    private final Parameters parameters;
    private final XMLEventFactory eventFactory;
    private final ZipFile zipFile;
    private final ZipEntry zipEntry;
    private final String parentId;
    private final String id;
    private final List<XMLEvent> events;
    private final List<Event> filterEvents;
    private IdGenerator documentPartIds;
    private IdGenerator textUnitIds;
    private List<XMLEvent> documentPartEvents;
    private Iterator<Event> filterEventsIterator;

    DesignMapSubDocument(
        final Parameters parameters,
        final XMLEventFactory eventFactory,
        final ZipFile zipFile,
        final ZipEntry zipEntry,
        final String parentId,
        final String id,
        final List<XMLEvent> events
    ) {
        this(
            parameters,
            eventFactory,
            zipFile,
            zipEntry,
            parentId,
            id,
            events,
            new LinkedList<>()
        );
    }

    DesignMapSubDocument(
        final Parameters parameters,
        final XMLEventFactory eventFactory,
        final ZipFile zipFile,
        final ZipEntry zipEntry,
        final String parentId,
        final String id,
        final List<XMLEvent> events,
        final List<Event> filterEvents
    ) {
        this.parameters = parameters;
        this.eventFactory = eventFactory;
        this.zipFile = zipFile;
        this.zipEntry = zipEntry;
        this.parentId = parentId;
        this.id = id;
        this.events = events;
        this.filterEvents = filterEvents;
    }

    @Override
    public Event open() throws IOException, XMLStreamException {
        this.documentPartIds = new IdGenerator(zipEntry.getName(), IdGenerator.DOCUMENT_PART);
        this.textUnitIds = new IdGenerator(zipEntry.getName(), IdGenerator.TEXT_UNIT);
        this.documentPartEvents = new LinkedList<>();
        readFilterEventsWith(new XMLEventsReader(this.events));
        this.filterEvents.add(new Event(EventType.END_SUBDOCUMENT, new Ending(this.id)));
        this.filterEventsIterator = this.filterEvents.iterator();
        return startSubDocumentEvent();
    }

    private void readFilterEventsWith(final XMLEventsReader reader) throws XMLStreamException {
        final TextElementMapping tem = new TextElementMapping(this.parameters, this.eventFactory, this.textUnitIds);
        final TextAttributeMapping tam = new TextAttributeMapping(this.textUnitIds);
        boolean inCustomTextVariablePreference = false;
        boolean inProperties = false;
        boolean inIndex = false;
        while (reader.hasNext()) {
            final XMLEvent e = reader.nextEvent();
            if (e.isStartElement()) {
                if (this.parameters.getExtractCustomTextVariables()
                    && inCustomTextVariablePreference && inProperties
                    && CONTENTS.equals(e.asStartElement().getName().getLocalPart())) {
                    flushDocumentPart();
                    final Element contents = new ElementParser(e.asStartElement(), reader, this.eventFactory).parse(new Element.Builder());
                    this.filterEvents.add(new Event(EventType.TEXT_UNIT, tem.textUnitFor(contents)));
                    continue;
                }
                if (this.parameters.getExtractIndexTopics()
                    && inIndex
                    && TOPIC.equals(e.asStartElement().getName().getLocalPart())) {
                    flushDocumentPart();
                    final Element topic = new ElementParser(e.asStartElement(), reader, this.eventFactory).parse(new Element.Builder());
                    this.filterEvents.add(new Event(EventType.TEXT_UNIT, tam.textUnitFor(topic, NAME)));
                    continue;
                }
                if (CUSTOM_TEXT_VARIABLE_PREFERENCE.equals(e.asStartElement().getName().getLocalPart())) {
                    inCustomTextVariablePreference = true;
                }
                if (PROPERTIES.equals(e.asStartElement().getName().getLocalPart())) {
                    inProperties = true;
                }
                if (INDEX.equals(e.asStartElement().getName().getLocalPart())) {
                    inIndex = true;
                }
            } else if (e.isEndElement()) {
                if (PROPERTIES.equals(e.asEndElement().getName().getLocalPart())) {
                    inProperties = false;
                }
                if (CUSTOM_TEXT_VARIABLE_PREFERENCE.equals(e.asEndElement().getName().getLocalPart())) {
                    inCustomTextVariablePreference = false;
                }
                if (INDEX.equals(e.asEndElement().getName().getLocalPart())) {
                    inIndex = false;
                }
            }
            this.documentPartEvents.add(e);
        }
        flushDocumentPart();
    }

    void flushDocumentPart() {
        this.filterEvents.add(
            new Event(
                EventType.DOCUMENT_PART,
                new DocumentPart(
                    this.documentPartIds.createId(),
                    false,
                    new MarkupSkeleton(
                        new Markup.Default(
                            Collections.singletonList(new MarkupRange.Default(this.documentPartEvents))
                        )
                    )
                )
            )
        );
        this.documentPartEvents = new LinkedList<>();
    }

    private Event startSubDocumentEvent() {
        final StartSubDocument sd = new StartSubDocument(this.parentId, this.id);
        sd.setName(this.zipEntry.getName());
        sd.setMimeType(TEXT_XML);
        final ZipSkeleton zs = new ZipSkeleton(this.zipFile, this.zipEntry);
        sd.setSkeleton(zs);
        sd.setFilterId(IDMLFilter.FILTER_ID);
        sd.setFilterParameters(this.parameters);
        return new Event(EventType.START_SUBDOCUMENT, sd);
    }

    @Override
    public boolean hasNextEvent() {
        return this.filterEventsIterator.hasNext();
    }

    @Override
    public Event nextEvent() {
        return this.filterEventsIterator.next();
    }

    @Override
    public void close() {
    }

    @Override
    public void logEvent(Event e) {
    }
}
