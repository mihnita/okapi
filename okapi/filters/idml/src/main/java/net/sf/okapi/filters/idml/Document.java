/*
 * =============================================================================
 *   Copyright (C) 2010-2013 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.idml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.skeleton.ZipSkeleton;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static java.util.Collections.enumeration;
import static java.util.Collections.list;

interface Document {
    Event open() throws XMLStreamException, IOException;
    boolean hasNextSubDocument();
    SubDocument nextSubDocument();
    void close() throws IOException;

    class Default implements Document {
        private final Parameters parameters;
        private final XMLInputFactory inputFactory;
        private final XMLOutputFactory outputFactory;
        private final XMLEventFactory eventFactory;
        private final String startDocumentId;
        private final URI uri;
        private final LocaleId sourceLocale;
        private final String encoding;
        private final String lineBreak;
        private final IFilterWriter filterWriter;

        private ZipFile zipFile;
        private ZipInput<XMLEventReader> zipInputReader;

        private DesignMapFragments designMapFragments;
        private StyleDefinitions styleDefinitions;
        private List<String> translatablePartNames;
        private Enumeration<? extends ZipEntry> zipFileEntries;
        private int currentSubDocumentId;

        Default(
            final Parameters parameters,
            final XMLInputFactory inputFactory,
            final XMLOutputFactory outputFactory,
            final XMLEventFactory eventFactory,
            final String startDocumentId,
            final URI uri,
            final LocaleId sourceLocale,
            final String encoding,
            final String lineBreak,
            final IFilterWriter filterWriter
        ) {
            this.parameters = parameters;
            this.inputFactory = inputFactory;
            this.outputFactory = outputFactory;
            this.eventFactory = eventFactory;
            this.startDocumentId = startDocumentId;
            this.uri = uri;
            this.sourceLocale = sourceLocale;
            this.encoding = encoding;
            this.lineBreak = lineBreak;
            this.filterWriter = filterWriter;
        }

        @Override
        public Event open() throws XMLStreamException, IOException {
            zipFile = new ZipFile(new File(uri.getPath()), ZipFile.OPEN_READ);

            final ZipInput<InputStream> zipInputStream = new ZipInput.Stream(this.zipFile);
            final MimeType mimeType = new MimeType(zipInputStream);
            mimeType.from(this.zipFile.getEntry(DesignMapFragments.MIME_TYPE));

            if (!IDMLFilter.MIME_TYPE.equals(mimeType.toString(this.encoding))) {
                throw new OkapiBadFilterInputException("IDML filter tried to initialise a file that is not supported.");
            }

            this.zipInputReader = new ZipInput.Reader(zipInputStream, this.encoding, this.inputFactory);

            this.designMapFragments = new DesignMapFragments.Default(this.parameters, eventFactory, this.zipFile, this.zipInputReader);
            this.designMapFragments.from(this.zipFile.getEntry(DesignMapFragments.DESIGN_MAP));

            this.styleDefinitions = new StyleDefinitions.Default(
                new Markup.Default(new LinkedList<>()),
                this.zipInputReader,
                this.eventFactory
            );
            this.styleDefinitions.from(this.zipFile.getEntry(this.designMapFragments.stylesPartName()));

            this.translatablePartNames = this.designMapFragments.translatablePartNames();
            this.zipFileEntries = zipFileEntries();
            currentSubDocumentId = 0;

            return getStartDocumentEvent(uri, sourceLocale, filterWriter);
        }

        private Enumeration<? extends ZipEntry> zipFileEntries() throws IOException, XMLStreamException {
            final List<? extends ZipEntry> entryList = list(zipFile.entries());
            entryList.sort(new ZipEntryComparator(this.designMapFragments.orderedPartNames()));
            return enumeration(entryList);
        }

        private Event getStartDocumentEvent(URI uri, LocaleId sourceLocale, IFilterWriter filterWriter) {
            StartDocument startDoc = new StartDocument(startDocumentId);
            startDoc.setName(uri.getPath());
            startDoc.setLocale(sourceLocale);
            startDoc.setMimeType(IDMLFilter.MIME_TYPE);
            startDoc.setFilterWriter(filterWriter);
            startDoc.setFilterId(IDMLFilter.FILTER_ID);
            startDoc.setFilterParameters(parameters);
            startDoc.setLineBreak(lineBreak);
            startDoc.setEncoding(encoding, false);  // IDML files don't have UTF8BOM
            ZipSkeleton skel = new ZipSkeleton(zipFile, null);

            return new Event(EventType.START_DOCUMENT, startDoc, skel);
        }

        @Override
        public boolean hasNextSubDocument() {
            return zipFileEntries.hasMoreElements();
        }

        @Override
        public SubDocument nextSubDocument() {
            final ZipEntry zipEntry = zipFileEntries.nextElement();

            if (!isTranslatableSubDocument(zipEntry.getName())) {
                if (isStylesSubDocument(zipEntry.getName())) {
                    return new MarkupModifiableSubDocument(
                        this.zipFile,
                        zipEntry,
                        this.outputFactory,
                        this.encoding,
                        this.styleDefinitions
                    );
                }
                return new NonModifiableSubDocument(zipFile, zipEntry);
            }

            if (DesignMapFragments.DESIGN_MAP.equals(zipEntry.getName())) {
                return new DesignMapSubDocument(
                    this.parameters,
                    this.eventFactory,
                    this.zipFile,
                    zipEntry,
                    this.startDocumentId,
                    String.valueOf(++this.currentSubDocumentId),
                    this.designMapFragments.events()
                );
            }
            return new StorySubDocument(
                this.parameters,
                this.eventFactory,
                this.zipFile,
                this.zipInputReader,
                zipEntry,
                this.startDocumentId,
                String.valueOf(++this.currentSubDocumentId)
            );
        }

        private boolean isTranslatableSubDocument(final String partName) {
            return this.translatablePartNames.contains(partName);
        }

        private boolean isStylesSubDocument(final String entryName) {
            return this.designMapFragments.stylesPartName().equals(entryName);
        }

        @Override
        public void close() throws IOException {
            zipFile.close();
        }

        private static class ZipEntryComparator implements Comparator<ZipEntry> {
            private final List<String> partNames;

            ZipEntryComparator(List<String> partNames) {
                this.partNames = partNames;
            }

            @Override
            public int compare(ZipEntry o1, ZipEntry o2) {
                int index1 = partNames.indexOf(o1.getName());
                int index2 = partNames.indexOf(o2.getName());

                if (index1 == -1) {
                    index1 = Integer.MAX_VALUE;
                }

                if (index2 == -1) {
                    index2 = Integer.MAX_VALUE;
                }

                return Integer.compare(index1, index2);
            }
        }
    }
}
