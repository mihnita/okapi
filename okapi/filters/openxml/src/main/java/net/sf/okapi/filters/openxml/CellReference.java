/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Objects;

final class CellReference {
    private final String string;
    private int row;
    private String column;
    private boolean split;

    CellReference(final String string) {
        this.string = string;
    }

    int row() {
        if (!this.split) {
            split();
        }
        return this.row;
    }

    String column() {
        if (!this.split) {
            split();
        }
        return this.column;
    }

    private void split() {
        final StringBuilder rowNumber = new StringBuilder();
        final StringBuilder columnName = new StringBuilder();
        final char[] chars = this.string.toCharArray();
        for (final char ch : chars) {
            if (Character.isDigit(ch)) {
                rowNumber.append(ch);
            } else {
                columnName.append(ch);
            }
        }
        this.row = Integer.parseUnsignedInt(rowNumber.toString());
        this.column = columnName.toString();
        this.split = true;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final CellReference that = (CellReference) o;
        return string.equals(that.string);
    }

    @Override
    public int hashCode() {
        return Objects.hash(string);
    }


    @Override
    public String toString() {
        return this.string;
    }
}
