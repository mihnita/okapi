/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

final class MarkupBuilder {
    private final Markup markup;
    private List<XMLEvent> events;

    MarkupBuilder(final Markup markup) {
        this.markup = markup;
        this.events = new ArrayList<>();
    }

    void add(final XMLEvent event) {
        this.events.add(event);
    }

    void addAll(final List<XMLEvent> events) {
        this.events.addAll(events);
    }

    void add(final MarkupComponent markupComponent) {
        flushEvents();
        this.markup.addComponent(markupComponent);
    }

    void add(final Markup markup) {
        flushEvents();
        this.markup.addMarkup(markup);
    }

    XMLEvent peekCurrentMarkupComponentEvent() {
        if (this.events.isEmpty()) {
            return null;
        }
        return this.events.get(
            this.events.size() - 1
        );
    }

    private void flushEvents() {
        if (!this.events.isEmpty()) {
            this.markup.addComponent(new MarkupComponent.General(this.events));
            this.events = new ArrayList<>();
        }
    }

    Markup build() {
        flushEvents();
        return this.markup;
    }
}
