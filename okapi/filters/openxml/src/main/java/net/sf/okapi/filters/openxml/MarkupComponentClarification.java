/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.Attribute;
import java.util.Collections;
import java.util.List;

/**
 * Provides a markup component clarification.
 */
interface MarkupComponentClarification {
    void performFor(final MarkupComponent markupComponent);

    final class Default implements MarkupComponentClarification {
        private final AttributesClarification attributesClarification;
        private final ElementsClarification elementsClarification;
        private final CharactersClarification charactersClarification;

        Default(final AttributesClarification attributesClarification) {
            this(
                attributesClarification,
                new ElementsClarification.Bypass(),
                new CharactersClarification.Bypass()
            );
        }

        Default(final ElementsClarification elementsClarification) {
            this(
                new AttributesClarification.Bypass(),
                elementsClarification,
                new CharactersClarification.Bypass()
            );
        }

        Default(final CharactersClarification charactersClarification) {
            this(
                new AttributesClarification.Bypass(),
                new ElementsClarification.Bypass(),
                charactersClarification
            );
        }

        Default(
            final AttributesClarification attributesClarification,
            final ElementsClarification elementsClarification
        ) {
            this(attributesClarification, elementsClarification, new CharactersClarification.Bypass());
        }

        Default(
            final AttributesClarification attributesClarification,
            final ElementsClarification elementsClarification,
            final CharactersClarification charactersClarification
        ) {
            this.attributesClarification = attributesClarification;
            this.elementsClarification = elementsClarification;
            this.charactersClarification = charactersClarification;
        }

        @Override
        public void performFor(final MarkupComponent markupComponent) {
            final List<Attribute> attributes;
            if (MarkupComponent.isStart(markupComponent)) {
                attributes = ((MarkupComponent.Start) markupComponent).getAttributes();
                this.attributesClarification.adjustClarificationContextWith(((MarkupComponent.Start) markupComponent).context());
            } else if (MarkupComponent.isBlockProperties(markupComponent)) {
                attributes = ((BlockProperties) markupComponent).attributes();
            } else if (MarkupComponent.isRunProperties(markupComponent)
                || MarkupComponent.isFormula(markupComponent)) {
                attributes = Collections.emptyList();
            } else {
                throw new IllegalArgumentException("Unsupported markup component: ".concat(markupComponent.getClass().getSimpleName()));
            }
            this.attributesClarification.performFor(attributes);
            if (MarkupComponent.isBlockProperties(markupComponent)) {
                this.elementsClarification.performFor(((BlockProperties) markupComponent).properties());
            }
            if (MarkupComponent.isRunProperties(markupComponent)) {
                this.elementsClarification.performFor(((RunProperties) markupComponent).properties());
            }
            if (MarkupComponent.isFormula(markupComponent)) {
                this.charactersClarification.adjustClarificationContextWith(((Formula) markupComponent).context());
                this.charactersClarification.performFor(((Formula) markupComponent).innerEvents());
            }
        }
    }
}
