package net.sf.okapi.common.resource;

public class CodeComparatorOnCloseType extends CodeComparatorOnType {
    @Override
    public int compare(Code c1, Code c2) {
        if (c1 == c2) {
            // these are not equal - only OPEN/CLOSE match
            return -1;
        }

        if (c1 == null || c2 == null) {
            return -1;
        }

        // ordered comparison. We assume c1 is OPEN!!
        if (c1.getTagType() == TextFragment.TagType.OPENING && c2.getTagType() == TextFragment.TagType.CLOSING) {
            return 0;
        }

        return -1;
    }
}
